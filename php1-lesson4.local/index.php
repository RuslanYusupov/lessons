<?php

    $messages = file(__DIR__ . '/messages.txt');

    if (isset($_POST) && !empty($_POST)) {
            $message[] = $_POST['message'] . "\n";
            file_put_contents(__DIR__ . '/messages.txt', $message, FILE_APPEND);
    }

?>
<!DOCTYPE html>
<html lang="ru">
    <head>
        <meta charset="UTF-8">
        <title>Гостевая книга</title>
        <link rel="stylesheet"
              href="/style.css"
              type="text/css">
    </head>
    <body>
    <form action="index.php"
          method="post">
        <fieldset>
            <legend>Гостевая книга</legend>

            <?php
                foreach ($messages as $write) {
                    echo $write . "<br><hr>";
                }
            ?>

            <textarea
                name="message"
                cols="100"
                rows="10"
                placeholder="&nbsp;Введите сообщение"
                autofocus></textarea>
            <br>
            <button type="submit">Отправить</button>
            <button type="reset">Очистить</button>
        </fieldset>
    </form>
    </body>
</html>