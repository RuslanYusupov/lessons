<?php
    $errors =[];
    if (isset($_FILES['file'])) {
        if (0 == $_FILES['file']['error'] && ('image/jpeg' == $_FILES['file']['type']) || 'image/png' == $_FILES['file']['type']) {
            $file = move_uploaded_file($_FILES['file']['tmp_name'], __DIR__ . '/files/' . $_FILES['file']['name']);
        } else {
            $errors[] = 'Неверный формат файла!';
        }
    }
?>
<!DOCTYPE html>
<html lang="ru">
    <head>
        <meta charset="UTF-8">
        <title>Загрузка файла</title>
        <link rel="stylesheet"
              href="/style.css"
              type="text/css">
    </head>
    <body>
        <form action="/download.php"
                method="post"
                enctype="multipart/form-data"
                name="file_form">
            <p>Добавить изображение (JPG или PNG)</p>
            <input type="file" name="file">
            <br>
            <br>
            <button type="submit" name="file">Отправить</button>
        </form>
        <br>

        <?php
        foreach ($errors as $error) {
            echo $error; ?><br><?php
        }
        ?>
    </body>
</html>