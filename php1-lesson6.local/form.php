<?php
    require __DIR__ . '/classes/GuestBook.php';
    require __DIR__ . '/classes/View.php';

    $form = new GuestBook();
    $form->append($_POST['message']);
    $form->save();

    $view = new View();
    $view->display('form.php');

    //require __DIR__ . '/templates/form.php';
