<?php

    require __DIR__ . '/TextFile.php';

    class GuestBook extends TextFile {

        public function append($text)
        {
            if (isset($_POST) && !empty($_POST)) {
                $this->messages[] = $text . "\n";
            }
        }

        public function save()
        {
            if (isset($_POST) && !empty($_POST)) {
                file_put_contents($this->way, $this->messages, FILE_APPEND);
            }
        }

    }
