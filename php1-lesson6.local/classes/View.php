<?php

class View
{

    protected $data;

    public function __construct()
    {

    }

    public function assign($name, $value)
    {
        $this->data[$name] = $value;
    }

    public function display($template)
    {
        require_once __DIR__ . '/../templates/' . $template;
    }

    public function render($template)
    {
        ob_start();
        require_once __DIR__ . '/../templates/' . $template;
        $rend = ob_get_contents();
        ob_end_clean();

        return $rend;

    }

}