<!DOCTYPE html>
<html lang="ru">
    <head>
        <meta charset="UTF-8">
        <title>PHP</title>
        <link rel="stylesheet"
              href="/style.css"
              type="text/css">
    </head>
    <body>
        <article>
            <section>
                <h1>Что возвращает оператор include, если его использовать как функцию?</h1>
                <ul>
                    <li>Обработка возвращаемых значений: оператор include возвращает значение FALSE при ошибке и выдает предупреждение.</li>
                    <li>Успешные включения, пока это не переопределено во включаемом файле, возвращают значение 1.</li>
                    <li>Возможно выполнить выражение <i>return</i> внутри включаемого файла, чтобы завершить процесс выполнения в этом файле и вернуться к выполнению включающего файла.</li>
                    <li>Также, возможно вернуть значение из включаемых файлов.</li>
                    <li>Можно получить значение включения как если бы вы вызвали обычную функцию.</li>
                </ul>
                <p>
                    Например:
                    <code>
                        <br>
                        <u>return.php</u><br>
                        &lt;?php<br>

                        $var = 'PHP';<br>

                        return $var;<br>

                        ?&gt;<br><br>

                        <u>noreturn.php</u><br>
                        &lt;?php<br>

                        $var = 'PHP';<br>

                        ?&gt;<br><br>

                        <u>testreturns.php</u><br>
                        &lt;?php<br>

                        $foo = include 'return.php';<br>

                        echo $foo; // выведет 'PHP'<br>

                        $bar = include 'noreturn.php';<br>

                        echo $bar; // выведет 1<br>

                        ?&gt;
                    </code>
                </p>
                <p>
                    $bar имеет значение 1, т.к. включение файла произошло успешно.<br>
                    Первый использует <i>return</i> внутри включаемого файла, тогда как второй не использует.<br>
                    Если файл не может быть включен, возвращается FALSE и возникает E_WARNING.
                </p>
                <a href="/index.php">&lt; Назад</a>
            </section>
        </article>
    </body>
</html>