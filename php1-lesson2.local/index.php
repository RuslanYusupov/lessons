<?php

    $a = true;
    $b = false;
?>
<!DOCTYPE html>
<html lang="ru">
    <head>
        <meta charset="UTF-8">
        <title>PHP</title>
        <link rel="stylesheet"
              href="/style.css"
              type="text/css">
    </head>
    <body>
    <ol>
        <li>
            <ul>
                <li>
                    <table>
                        <tr>
                            <th>&&</th>
                            <th><?php echo (int)$a; ?></th>
                            <th><?php echo (int)$b; ?></th>
                        </tr>
                        <tr>
                            <th><?php echo (int)$a; ?></th>
                            <th>
                                <?php echo (int)($a && $a); ?>
                            </th>
                            <th>
                                <?php echo (int)($a && $b); ?>
                            </th>
                        </tr>
                        <tr>
                            <th><?php echo (int)$b; ?></th>
                            <th>

                                <?php echo (int)($b && $a); ?>
                            </th>
                            <th>
                                <?php echo (int)($b && $b); ?>
                            </th>
                        </tr>
                    </table>
                </li>
                <li>
                    <table>
                        <tr>
                            <th>||</th>
                            <th><?php echo (int)$a; ?></th>
                            <th><?php echo (int)$b; ?></th>
                        </tr>
                        <tr>
                            <th><?php echo (int)$a; ?></th>
                            <th>
                                <?php echo (int)($a || $a); ?>
                            </th>
                            <th>
                                <?php echo (int)($a || $b); ?>
                            </th>
                        </tr>
                        <tr>
                            <th><?php echo (int)$b; ?></th>
                            <th>

                                <?php echo (int)($b || $a); ?>
                            </th>
                            <th>
                                <?php echo (int)($b || $b); ?>
                            </th>
                        </tr>
                    </table>
                </li>
                <li>
                    <table>
                        <tr>
                            <th>xor</th><!--Исключающее или-->
                            <th><?php echo (int)$a; ?></th>
                            <th><?php echo (int)$b; ?></th>
                        </tr>
                        <tr>
                            <th><?php echo (int)$a; ?></th>
                            <th>
                                <?php echo (int)($a xor $a); ?>
                            </th>
                            <th>
                                <?php echo (int)($a xor $b); ?>
                            </th>
                        </tr>
                        <tr>
                            <th><?php echo (int)$b; ?></th>
                            <th>

                                <?php echo (int)($b xor $a); ?>
                            </th>
                            <th>
                                <?php echo (int)($b xor $b); ?>
                            </th>
                        </tr>
                    </table>

                    <p>
                    <strong>XOR</strong> (Исключающее ИЛИ) - TRUE, когда первый, или второй операнд = TRUE, но не оба.
                    </p>

                </li>
            </ul>
        </li>

        <li><a href="/quadratic-equation.php">Квадратное уравнение</a></li>
        <li><a href="/include.php">Include</a></li>
        <li><a href="/gender.php">Определение пола</a></li>
    </ol>
    </body>
</html>