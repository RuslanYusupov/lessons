<?php

    if (isset($_POST) && !empty($_POST)) {
        $x = $_POST['name'];
    }

    require __DIR__ . '/function.php';
    require __DIR__ . '/tests.php';

?>
<!DOCTYPE html>
<html lang="ru">
    <head>
        <meta charset="UTF-8">
        <title>Пол</title>
        <link rel="stylesheet"
              href="/style.css"
              type="text/css">
    </head>
    <body
        class="gender">
        <form
            action="/gender.php"
            method="post">
            <fieldset>
                <legend>Определение пола по имени</legend>
                <input type="text"
                       name="name"
                       placeholder="ИМЯ"
                       value="<?php echo $_POST['name']; ?>"
                       pattern="^[А-Яа-яЁё]+$"
                       required>
                <button type="submit">Определить</button>
            </fieldset>
        </form>
        <p>
            Ваш пол: <?php echo gender($x); ?>
        </p>
        <a href="/index.php">&lt; Назад</a>
    </body>
</html>