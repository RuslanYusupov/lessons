<?php
    function discriminant($a, $b, $c)
    {
        if (isset($_POST) && !empty($_POST)) {

            $d = $b**2 - 4 * $a * $c;

            if ($a == 0 && $b == 0) {
                return 'один корень: ' . 0;
            } elseif ($a == 0) {
                $x0 = (-$c) / ($b);
                return 'один корень: ' . $x0;
            } elseif ($b == 0 && $c != 0) {
                $x0 = ($c / $a) ** 0.5;
                return 'два корня: ' . $x0 . ' и ' . -$x0;
            }

            switch (true) {
                case ($d < 0):
                    return 'корней нет';
                    break;
                case ($d == 0):
                    $x0 = (-$b) / (2 * $a);
                    return 'один корень: ' . $x0;
                    break;
                case ($d > 0):
                    $x1 = ((-$b) + $d ** 0.5) / (2 * $a);
                    $x2 = ((-$b) - $d ** 0.5) / (2 * $a);
                    return 'два корня: ' . $x1 . ' и ' . $x2;
                    break;
            }
        } else {
            return 'введите значения';
        }
    }

    function gender($x)
    {
        if (isset($_POST) && !empty($_POST)) {

            if (strrpos($x, 'а', strlen($x)-2) !== false || strrpos($x, 'я', strlen($x)-2) !== false) {
                return 'женский';
            } else {
                return 'мужской';
            }

        } else {
            echo 'введите полное имя';
        }
    }