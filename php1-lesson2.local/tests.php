<?php

    assert('один корень: 0' == discriminant(0, 0, 0));
    assert('корней нет' == discriminant(1, 1, 1));
    assert('один корень: -0.5' == discriminant(4, 4, 1));
    assert('мужской' == gender('Игорь'));
    assert('женский' == gender('Алиса'));
    assert('мужской' == gender('Александр'));
