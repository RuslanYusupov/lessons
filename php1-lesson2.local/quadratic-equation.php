<?php

    if (isset($_POST) && !empty($_POST)) {
        $a = $_POST['a'];
        $b = $_POST['b'];
        $c = $_POST['c'];
    }
    require __DIR__ . '/function.php';
    require __DIR__ . '/tests.php';

?>
<!DOCTYPE html>
<html lang="ru">
    <head>
        <meta charset="UTF-8">
        <title>Квадратное уравнение</title>
        <link rel="stylesheet"
              href="/style.css"
              type="text/css">
    </head>
    <body
        class="qe">
        <form action="/quadratic-equation.php"
            method="post">
            <fieldset>
                <legend>Квадратное уравнение</legend>

                <input type="number"
                       step="any"
                       name="a"
                       value="<?php echo $_POST['a']; ?>"
                       placeholder="a"
                       required>&nbsp;&nbsp;x<sup>2</sup>&nbsp;
                <input type="number"
                       step="any"
                       name="b"
                       value="<?php echo $_POST['b']; ?>"
                       placeholder="b"
                       required>&nbsp;&nbsp;x&nbsp;
                <input type="number"
                       step="any"
                       name="c"
                       value="<?php echo $_POST['c']; ?>"
                       placeholder="c"
                       required> = 0
                <button type="submit">Решить</button>
            </fieldset>
        </form>

        <p>
            Ответ:
            <?php echo discriminant($a, $b, $c); ?>
        </p>
        <a href="/index.php">&lt; Назад</a>
    </body>
</html>