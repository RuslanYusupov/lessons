<?php
    $operations = ['+', '-', '*', '/'];

    $x = $_GET['x'];
    $y = $_GET['y'];
    $operation = $_GET['operation'];

    include __DIR__ . '/functions.php';
?>
<!DOCTYPE html>
<html lang="ru">
    <head>
        <meta charset="UTF-8">
        <title>Калькулятор</title>
        <link rel="stylesheet"
              href="/css/style.css"
              type="text/css">
    </head>
    <body>
    <form action="/calc.php">
        <fieldset>
            <legend>Калькулятор</legend>
            <input
                type="number"
                step="any"
                name="x"
                value="<?php echo $_GET['x']; ?>" required>

            <select name="operation">

                <?php foreach ($operations as $op): ?>

                <option value="<?php echo $op; ?>"

                        <?php
                            if ($op == $_GET['operation']) {
                                echo 'selected';
                            }
                        ?>

                    ><?php echo $op; ?>

                </option>

                <?php endforeach; ?>

            </select>

            <input
                type="number"
                step="any"
                name="y"
                value="<?php echo $_GET['y']; ?>" required>

            <a href="/calc.php?x=<?php echo $_GET['x']; ?>&<?php echo $_GET['y']; ?>">
                <button type="submit">=</button>
            </a>

            <?php
                echo calc($x, $y, $operation);
            ?>

        </fieldset>
    </form>
    </body>
</html>