<?php
    //Третий способ

    $dir = scandir(__DIR__ . '/../img');

    $picture = array_intersect($dir, $_GET);

    function pic($picture)
    {
        foreach ($picture as $pic) {
            return $pic;
        }
    }

?>
<!DOCTYPE html>
<html lang="ru">
    <head>
        <meta charset="UTF-8">
        <title>Картинка <?php echo pic($picture); ?></title>
        <link rel="stylesheet"
              href="/css/style2.css"
              type="text/css">
    </head>
    <body>
    <h1>Картинка <?php echo pic($picture); ?></h1>
    <a href="/gallery3/gallery3.php">Назад</a>
    <br>

    <img src="/img/<?php echo pic($picture); ?>"
         alt="Картинка <?php echo pic($picture); ?>">

    </body>
</html>