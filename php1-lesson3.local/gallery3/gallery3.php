<?php
    //Третий способ

    $dir = scandir(__DIR__ . '/../img');
    var_dump($dir);

?>
<!DOCTYPE html>
<html lang="ru">
    <head>
        <meta charset="UTF-8">
        <title>Галерея</title>
        <link rel="stylesheet"
              href="/css/style2.css"
              type="text/css">
    </head>
    <body>
    <h1>Галерея изображений</h1>

    <?php foreach($dir as $pic): ?>

        <?php if ($pic != '.' && $pic != '..'): ?>
            <a href="/gallery3/image3.php?id=<?php echo $pic?>"
               target="_blank">
                <img
                    src="/img/<?php echo $pic?>"
                    alt="Картинка <?php echo $pic?>">
            </a>
        <?php endif; ?>

        <br>

    <?php endforeach; ?>

    </body>
</html>