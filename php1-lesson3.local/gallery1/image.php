<?php
    //Первый способ (Домашнее задание)

    $pictures = [1 => 'img1.jpg', 2 => 'img2.jpg', 3 => 'img3.jpg'];

?>
<!DOCTYPE html>
<html lang="ru">
    <head>
        <meta charset="UTF-8">
        <title>Картинка N <?php echo $_GET['id']; ?></title>
        <link rel="stylesheet"
              href="/css/style2.css"
              type="text/css">
    </head>
    <body>
    <h1>Картинка N <?php echo $_GET['id']; ?></h1>
    <a href="/gallery1/gallery.php">Назад</a>
    <br>

    <img src="/img/<?php echo $pictures[$_GET['id']]; ?>"
         alt="Картинка N <?php echo $_GET['id']; ?>">

    </body>
</html>