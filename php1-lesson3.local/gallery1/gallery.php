<!DOCTYPE html>
<html lang="ru">
    <head>
        <meta charset="UTF-8">
        <title>Галерея</title>
        <link rel="stylesheet"
              href="/css/style2.css"
              type="text/css">
    </head>
    <body>
        <h1>Галерея изображений</h1>

        <a href="/gallery1/image.php?id=1"
           target="_blank">
            <img
                src="/img/img1.jpg"
                alt="Картинка 1">
        </a>
        <br>
        <a href="/gallery1/image.php?id=2"
           target="_blank">
            <img
                src="/img/img2.jpg"
                alt="Картинка 2">
        </a>
        <br>
        <a href="/gallery1/image.php?id=3"
           target="_blank">
            <img
                src="/img/img3.jpg"
                alt="Картинка 3">
        </a>

    </body>
</html>