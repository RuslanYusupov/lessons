<!DOCTYPE html>
<html lang="ru">
    <head>
        <meta charset="UTF-8">
        <title>Картинка N <?php echo $_GET['id']; ?></title>
        <link rel="stylesheet"
              href="/css/style2.css"
              type="text/css">
    </head>
    <body>
    <h1>Картинка N <?php echo $_GET['id']; //На второй способ не похоже, но кода меньше. ?></h1>
    <a href="/gallery2/gallery2.php">Назад</a>
    <br>

    <img src="/img/img<?php echo $_GET['id']; ?>.jpg"
         alt="Картинка N <?php echo $_GET['id']; ?>">

    </body>
</html>