<?php

    class TextFile {

        protected $way = __DIR__ . '/../messages/user_messages.txt';
        protected $messages;

        public function __construct()
        {
            $this->messages = file($this->way);
        }

        public function getData()
        {
            return $this->messages;
        }

    }
