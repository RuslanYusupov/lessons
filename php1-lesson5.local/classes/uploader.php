<?php

    class Uploader {

        protected $name;

        public function __construct()
        {
            $this->name = $_FILES['file'];
        }

        public function isUploaded()
        {
            if (in_array($this->name, $_FILES) && 0 == $_FILES['file']['error']) {
                return $this;
            }
        }

        public function upload()
        {
            if ($this->isUploaded() == $this) {
                move_uploaded_file($_FILES['file']['tmp_name'], __DIR__ . '/../files/' . $_FILES['file']['name']);
            }
        }
    }