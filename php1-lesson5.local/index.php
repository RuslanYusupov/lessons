<?php

    class Item {

        public $color;
        public $legs;

        public function  showColorAndLegs()
        {
            echo 'Мой цвет: ' . $this->color . "<br>";
            echo 'Количество ножек: ' . $this->legs . "<br>";
        }

    }

    class Table extends Item {

        public function __construct($color, $legs=4)
        {
            $this->legs = $legs;
            $this->color = $color;
        }

    }

$table = new Table('black', 5);
$table->showColorAndLegs();
var_dump($table);
