<?php

    require __DIR__ . '/classes/uploader.php';

    $download = new Uploader();
    $download->isUploaded();
    $download->upload();

?>
<!DOCTYPE html>
<html lang="ru">
    <head>
        <meta charset="UTF-8">
        <title>Загрузка файла</title>
        <link rel="stylesheet"
              href="/style.css"
              type="text/css">
    </head>
    <body>
        <form action="/download.php"
                method="post"
                enctype="multipart/form-data"
                name="file_form">
            <p>Добавьте изображение</p>
            <input type="file" name="file">
            <br>
            <br>
            <button type="submit" name="file_button">Отправить</button>
        </form>
    </body>
</html>